<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'users/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['users/login'] = "users/login";
$route['users/add'] = "users/add";
$route['users/profile'] = "users/profile";
$route['users/reset-users/(:num)'] = "users/reset_users/$1";
//$route['users/updateuserstatus/(:num)'] = "users/updateuserstatus/$1";
//$route['users/get123/(:num)'] = "users/get123/$1";

$route['groups/index'] = "groups";
$route['groups/index'] = "groups/index";
$route['groups/add'] = "groups/add";
$route['groups/update/(:any)'] = 'groups/update/$1';
$route['groups/delete'] = "groups/delete";

$route['group-users/reassing/(:num)'] = "group_users/reassing/$1";
//$route['group-users/search-assing/(:num)'] = "group_users/search_assing/$1";
//$route['group-users/assing-users/(:num)'] = "group_users/assing_users/$1";
$route['group-users/reassing/(:num)/(:num)'] = "group_users/reassing/$1/$2";
$route['group-users/getcsv/(:num)'] = "group_users/getcsv/$1";
$route['group-users/getcsv'] = "group_users/getcsv";

$route['meeting/(:any)/(:any)'] = "group_users/meeting/$1/$2";




/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['meetings/index'] = "meetings/index";
$route['api/visiting/(:num)/(:num)/(:any)'] = 'meetings/index/$1/$2/$3'; // Example 8

//$route['api/visiting'] = 'meetings/index'; // Example 8


// added ifoundries - Wesley
// sync users data from ProtegeGX to database
$route['gx-users/refresh'] = "gx_users/refresh";
$route['gx-users/do-refresh'] = "gx_users/doRefresh";
$route['group-users/assign/(:num)'] = "group_users/assign/$1";
$route['group-users/dissociate/(:num)'] = "group_users/dissociate/$1";
// eo ifoundries - Wesley
