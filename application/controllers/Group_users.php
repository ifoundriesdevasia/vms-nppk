<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//date_default_timezone_set('America/New_York');

class Group_Users extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->output->set_template('default');
		$this->load->model('group', '', TRUE);
		$this->load->model('group_user', '', TRUE);
		$this->load->model('gx_user_model', '', TRUE);
		$this->load->model('Permission_Model', 'permissions', true);

		if (!$this->session->userdata('logged_in')) {
			// Allow some methods?
			$allowed = array('meeting', 'visiting');
			if (!in_array($this->router->fetch_method(), $allowed)) {
				redirect('users/login');
			}
		}
	}

	/**
	 * This page has moved to new location.
	 */
	public function getcsv() {
		$id = null;
		if ($this->uri->segment(3)) {
			$id = $this->uri->segment(3);
		}

		redirect("groups/export/{$id}");
	}

	public function assign() {
		$user = $this->session->userdata('logged_in');
		$data = $this->permissions->edit($user['isSuper']);
		$this->page = 'group';
		chkaccess($data, $this->page, 'assigning');


		$id = $this->uri->segment(3);
		if (empty($id)) {
			redirect('groups');
		}

		$groupData = $this->group->getData($id);

		// group valid?
		if (empty($groupData)) {
			$this->session->set_flashdata('error', 'Group not found');
			redirect('groups');
		}

		$ids = $this->input->post('assign');

		if (empty($ids)) {
			$this->session->set_flashdata('info', 'No user has been assigned.');
		}
		else {
			$result = $this->group_user->assignGroup($id, $ids);

			if (empty($result['total'])) {
				$this->session->set_flashdata('info', 'No user has been assigned.');
			}
			else {
				if ($result['success'] > 0) {
					$this->session->set_flashdata('success', "{$result['success']} user(s) has been assigned.");
				}

				if ($result['fail'] > 0) {
					$this->session->set_flashdata('error', "{$result['fail']} user(s) failed to assign.");
				}

				if ($result['other'] > 0) {
					$this->session->set_flashdata('warning', "{$result['other']} user(s) exists in another group.");
				}
			}
		}

		// set form data for redirect
		$postDataSearch = $this->input->post_get('formdata');
		$toUrl = $this->_attachFormDataLink("group-users/reassing/{$id}", is_null($postDataSearch)? array() : $postDataSearch);

		// redirect
		redirect($toUrl);
	}

	public function dissociate() {
		$id = $this->uri->segment(3);
		if (empty($id)) {
			redirect('groups');
		}

		$groupData = $this->group->getData($id);

		// group valid?
		if (empty($groupData)) {
			$this->session->set_flashdata('error', 'Group not found');
			redirect('groups');
		}

		// user IDs
		$ids = $this->input->post('dissociate');

		if (empty($ids)) {
			$this->session->set_flashdata('info', 'No user has been unassigned.');
		}
		else {
			$result = $this->group_user->dissociateGroup($id, $ids);

			if (empty($result['total'])) {
				$this->session->set_flashdata('info', 'No user has been unassigned.');
			}
			else {
				if ($result['success'] > 0) {
					$this->session->set_flashdata('success', "{$result['success']} user(s) has been unassigned.");
				}

				if ($result['fail'] > 0) {
					$this->session->set_flashdata('error', "{$result['fail']} user(s) failed to unassign.");
				}
			}
		}


		// set form data for redirect
		$postDataSearch = $this->input->post_get('formdata');
		$toUrl = $this->_attachFormDataLink("group-users/reassing/{$id}", is_null($postDataSearch)? array() : $postDataSearch);

		// redirect
		redirect($toUrl);
	}

	public function reassing($id = null) {
		$user = $this->session->userdata('logged_in');
		$data['userrole'] = $this->permissions->edit($user['isSuper']);
		$this->page = 'group';
		// chkaccess($data,$this->page,'assigning');

		// group ID
		if (is_null($id)) {
			$id = $this->uri->segment(3);
		}

		if (empty($id)) {
			redirect('groups');
		}

		// get segment data
		$segmentData = method_exists($this->uri, 'segment_data')? $this->uri->segment_data() : array();

		// current group data
		$data['group'] = $this->group->getData($id);

		// group valid?
		if (empty($data['group'])) {
			$this->session->set_flashdata('error', 'Group not found');
			redirect('groups');
		}

		$this->output->set_template('default');
//		$this->output->set_title('Singapore Police Force');

		$toUrl = "group-users/reassing/{$id}";

		// group ID
		$data['group_id'] = $id;

		// form search data
		$postDataSearch = $this->input->post('form');
		if (empty($postDataSearch) && !empty($segmentData['formdata'])) {
			$tmp = $segmentData['formdata'];
			if (is_string($tmp) && $tmp) {
				$tmp = json_decode(base64_decode($tmp), true);
				if (is_array($tmp)) {
					$postDataSearch = $tmp;

					// set POST
					$_POST['form'] = $postDataSearch;
				}
			}
		}

		$data['formdata'] = $postDataSearch? base64_encode(json_encode($postDataSearch)) : '';


		// get users & pagination links
		$perpage = 30;
		$pageOffset = (!empty($segmentData['ustart']) && is_numeric($segmentData['ustart']) && $segmentData['ustart'] > 0)? (int) $segmentData['ustart'] : 0;
		$pgLinks = $this->_attachFormDataLink($toUrl, $postDataSearch);
		$data['all_users_total'] = $this->gx_user_model->getUsersTotalWithGroup($postDataSearch);
		$data['all_users'] = $this->gx_user_model->getUsersWithGroup($postDataSearch, $perpage, $pageOffset);
		$data['all_users_pagelinks'] = WHelper::getPaginationHtml(site_url($pgLinks . '/'), $data['all_users_total'], $perpage, 5, ['prefix' => 'ustart:']);

		// users assign in current group
		$data['group_users'] = $this->group_user->get_data($id);


		// form data
		$ddlAttr = array(
			'class' => 'select2 form-control',
			'data-minimum-results-for-search' => '10',
		);

		$formdata = array(
			'designation' => array_merge(array('' => '- Select Option -'), $this->gx_user_model->getCfEntriesDesignation()),
			'department' => array_merge(array('' => '- Select Option -'), $this->gx_user_model->getCfEntriesDepartment()),
			'division' => array_merge(array('' => '- Select Option -'), $this->gx_user_model->getCfEntriesDivision()),
			'team' => array_merge(array('' => '- Select Option -'), $this->gx_user_model->getCfEntriesTeam()),
			'company' => array_merge(array('' => '- Select Option -'), $this->gx_user_model->getCfEntriesCompany()),
			'visitor' => array_merge(array('' => '- Select Option -'), $this->gx_user_model->getCfEntriesVisitor()),
			'search_scope' => array(
				array('name' => 'Name'),
				array('cf_nric' => 'NRIC'),
				array('card_number' => 'Card Number'),
			),
		);

		$data['formhtml'] = array(
			'search' => form_input('form[search]', set_value('form[search]', ''), array('class' => 'form-control', 'placeholder' => 'Search Keywords')),
//			'search_scope'	=> form_dropdown('form[search_scope]', $formdata['search_scope'], set_value('form[search_scope]', ''), $ddlAttr2),
			'search_scope' => form_radios('form[search_scope]', $formdata['search_scope'], set_value('form[search_scope]', 'name'), array('labelclass' => 'radio-inline')),
			'designation' => form_dropdown('form[designation]', $formdata['designation'], set_value('form[designation]', ''), $ddlAttr),
			'department' => form_dropdown('form[department]', $formdata['department'], set_value('form[department]', ''), $ddlAttr),
			'division' => form_dropdown('form[division]', $formdata['division'], set_value('form[division]', ''), $ddlAttr),
			'team' => form_dropdown('form[team]', $formdata['team'], set_value('form[team]', ''), $ddlAttr),
			'company' => form_dropdown('form[company]', $formdata['company'], set_value('form[company]', ''), $ddlAttr),
			'visitor' => form_dropdown('form[visitor]', $formdata['visitor'], set_value('form[visitor]', ''), $ddlAttr),
		);

		$data['groupInfoUrl'] = site_url('groups/groupTapInfo/?loaduser=1');

		$this->load->view('group_users/reassing', $data);
	}

	/**
	 * Search on user list.
	 */
	public function search() {
		// group ID
		$id = $this->uri->segment(3);
		if (empty($id)) {
			redirect('groups');
		}

		$toUrl = "group-users/reassing/{$id}";

		// form search data
		$postData = $this->input->post('form');

		redirect($this->_attachFormDataLink($toUrl, is_null($postData)? array() : $postData));
	}

	/**
	 * Attach form data to given link.
	 * @param string $link
	 * @param mixed $data
	 * @return string
	 */
	protected function _attachFormDataLink($link, $data=null) {
		$formdata = '';
		if(is_null($data)) {
			// get segment data
			$segmentData = method_exists($this->uri, 'segment_data')? $this->uri->segment_data() : array();

			// check if there is formdata in segment
			if (!empty($segmentData['formdata'])) {
				$formdata = $segmentData['formdata'];
			}
		}
		else {
			if(is_array($data)) {
				foreach($data as $k => $v) {
					if(is_string($v) && '' === $v) {
						unset($data[$k]);
					}
				}

				$data = base64_encode(json_encode($data));
			}

//			$formdata = is_string($data)? $data : base64_encode(json_encode($data));
			$formdata = is_string($data)? $data : '';
		}

		return rtrim($link, '/') . '/' . "formdata:{$formdata}";
	}

}
