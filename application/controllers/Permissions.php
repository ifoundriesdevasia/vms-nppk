<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends CI_Controller {
    private $data;
    private $page;
    /**
    @param void
    @return void
    */
    function __construct(){
        parent::__construct();
        if(!$this->session->userdata('logged_in')){
            redirect(base_url('users/login'));
        }
	    $permissions = $this->load->model('Permission_Model','permissions');
    }

    /**
    @param void
    @return void
    */
    public function index(){
        $this->data = array(

			'isAdmin'	=> !empty($user['isSuper']),

		);
        $this->data['permissions'] = $this->permissions->get();
        $this->output->set_template('default');
        $this->load->view('permissions/list', $this->data);
    }

    public function edit($id){
        $this->data['id'] = $id;
        $this->data['permission'] = $this->permissions->edit($id);
        $this->output->set_template('default');
        $this->load->view('permissions/update',$this->data);
    }

    public function update(){
    	foreach($_POST['permissions'] as $key=>$val){
    		$permission['permissions'] = json_encode($val);
        	$response = $this->permissions->update($key,$permission);
    	}
        $this->session->set_flashdata('success', $response['message']);
        redirect(base_url('permissions'));
    }

    public function check_role_name($name) {    
         if($this->data['id'])
            $id = $this->data['id'];
        else
            $id = ' ';
        $result = $this->rolepermission->check_unique_user_name($id, $name);
        if($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_role_name', 'Name must be unique');
            $response = false;
        }
        return $response;

    }
    public function destroy($id){
        if($id <= 3){
        $this->session->set_flashdata('error', 'You can not delete this role.');
        redirect(base_url('admin/RolePermission/create'));
        }
        chkaccess($this->data,$this->page,'delete');        

        $response = $this->rolepermission->destroy($id);
        $status = $response['status']===true?'success':'error';
        $this->session->set_flashdata($status, $response['message']);
        redirect(base_url('admin/RolePermission'));
    }

}
