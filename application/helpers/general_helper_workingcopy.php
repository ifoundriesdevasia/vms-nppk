<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */



// ------------------------------------------------------------------------



/**
 * CodeIgniter Email Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/email_helper.html
 */



// ------------------------------------------------------------------------



/**
 * Validate email address
 *
 * @access	public
 * @return	bool
 */

 

 

 	/**
	* Method Name : is_signed_in 
	* Parameter : NULL
	* Task : Loading for checking the user logged in into cloud or not
	*/

	if ( ! function_exists('is_signed_in'))
	{	
		function is_signed_in(){
			$CI =& get_instance();
			return $CI->session->userdata('logged_in') ? TRUE : FALSE;
		}
	}


	/**
	* Method Name : userId 
	* Parameter : NULL
	* Task : Loading for getting the userid
	*/

	if ( ! function_exists('userId'))
	{	
		function userId(){
			$CI =& get_instance();
			return $CI->session->userdata('user_id');
		}
	}	

	/**
	* Method Name : IsSuperAdmin
	* Parameter : NULL
	* Task : Loading for cechking the user super admin or not
	*/

	if ( ! function_exists('IsSuperAdmin')){
		function IsSuperAdmin(){
			$CI =& get_instance();
			$role_id=$CI->session->userdata('user_role_id');
			if($role_id==ROLE_ID_SUPERADMIN){
				return true;
			}else{
				return false;
			}
		}
	}
			

if ( ! function_exists('setActive'))
{
	function setActive($menuItem="")
	{
		if($menuItem=="")return "";
		$CI =& get_instance();
		 $selectedMenu=$CI->session->userdata('selectedMenu');
		if($selectedMenu==$menuItem){
			return "active";
		}else{
			return "";
		}
	}
}

 /**
	* Method Name : flash_message 
	* Parameter : null
	* This is using for show message from session and clear message from session
	*/

	if ( ! function_exists('flash_message')){
		function flash_message(){
			$CI =& get_instance();
			$message=$CI->session->userdata('error_message');
			$CI->session->set_userdata('error_message','');
			$format_error=$message;
			return $format_error;
		}
	}


/**
	* Method Name : set_flash_message 
	* Parameter : message,message_type
	* This is using for set message in session for show message
	*/

	if ( ! function_exists('set_flash_message')){
		function set_flash_message($message,$message_type='success'){
			$CI =& get_instance();
			$message=$CI->session->set_userdata('error_message',"<div class='alert alert-$message_type'><a href='#' data-dismiss='alert' class='close'>×</a><strong>$message</strong></div>");
		}
	}


/**
	* Method Name : current_datetime 
	* Parameter : time=true/false
	* This is using for return current date time
	*/

	if ( ! function_exists('current_datetime')){
		function current_datetime($time=false){
			if($time==true){
				return date("Y-m-d H:i:s");
			}else{
				return date("Y-m-d");
			}			
		}
	}

	/**
	* Method Name : dateFormat 
	* Parameter : date
	* This is using for convert date in format "Aug 20, 2015"
	*/
	 
	if ( ! function_exists('dateFormat')){
		function dateFormat($date){
			return date('M d, Y', strtotime($date));
		}
	}

	/**
	* Method Name : dateFormatTime 
	* Parameter : date
	* This is using for convert date with time in format "Aug 20, 2015 01:25:25 PM"
	*/

	if ( ! function_exists('dateFormatTime')){
		function dateFormatTime($date){
			return date('M d, Y h:i:s A', strtotime($date));
		}
	}

	/**
	* Method Name : formDisplayError 
	* Parameter : fieldName
	* This is using for create forms display errors format
	*/

	if (!function_exists('formDisplayError')){
		function formDisplayError($fieldName){
			return form_error($fieldName, "<div class='alert alert-danger'><a href='#' data-dismiss='alert' class='close'>×</a><strong>","</strong></div>");
		}
	}

	/**
	* Method Name : encodeId 
	* Parameter : ciperText
	* This is using for encode string or id
	*/

	if ( ! function_exists('encodeId')){
		function encodeId($ciperText){
			$CI =& get_instance();
	 		$encryptedCode=$CI->encrypt->encode($ciperText);
			$encryptedCode=str_replace("/", ":", $encryptedCode);
			return ($encryptedCode);			
		}
	}

	/**
	* Method Name : decodeId 
	* Parameter : encodedText
	* This is using for decode string or id
	*/

	if ( ! function_exists('decodeId')){
		function decodeId($encodedText){
			$CI =& get_instance();
			$encodedText=str_replace(":", "/", $encodedText);
			$decodedCode=$CI->encrypt->decode($encodedText);
			return ($decodedCode);
		}
	}

	/*
	* Method Name : ImagePath
	* Parameter : null
	* Task : Loading view for return theme images path
	*/

	if ( ! function_exists('ImagePath')){
		function ImagePath(){
			return base_url().IMAGES_PATH;
		}
	}

	/*
	* Method Name : createUploadDirectories
	* Parameter : null
	* Task : Loading view for create directries for upload
	*/

	if ( ! function_exists('createUploadDirectories')){
		function createUploadDirectories($upload_path=null){
			if($upload_path==null) return false;
			$upload_directories = explode('/',$upload_path);
			$createDirectory = array();
			foreach ($upload_directories as $upload_directory){
				$createDirectory[] = $upload_directory;
				$createDirectoryPath = implode('/',$createDirectory);
				if(!is_dir($createDirectoryPath)){
					$old = umask(0); 
					mkdir($createDirectoryPath,DIR_WRITE_MODE);// Create the folde if not exist and give permission
					umask($old); 
				}				
			}
			return true;
		}
	}

	/**
	* Method Name : is_logged_in 
	* Parameter : NULL
	* Task : Loading for checking the user logged in into frontend or not
	*/

	if ( ! function_exists('is_logged_in'))
	{	
		function is_logged_in(){
			$CI =& get_instance();
			return $CI->session->userdata('logged_in') ? TRUE : FALSE;
		}
	}
	
	/**
	* Method Name : getConfigItem 
	* Parameter : NULL
	* Task : Loading for return config item value
	*/
	if(!function_exists('getConfigItem')){
		function getConfigItem($item_name=false){
			if($item_name==false) return false;
			$CI =& get_instance();
			return $CI->config->item($item_name);
		}
	}
	
	
	function getLeadUserDetail($crm_user_id=false){
			if($crm_user_id==false) return array();
			$CI =& get_instance();
			$CI->load->library('SugarCRM');
			$sugar= new Rest;
			$sugar->setUrl(getConfigItem('crm_url'));
			$sugar->setUsername(getConfigItem('crm_username'));
			$sugar->setPassword(getConfigItem('crm_password'));
			$sugar->connect();
			$error = $sugar->get_error();
			//checking for the error if coming
			if($error !== FALSE) {
				print_r($error);
			}
			$results = $sugar->get_by_id($crm_user_id,'Leads',array());
			if(isset($results[0]['first_name'])){
				$amount = isset($results[0]['opportunity_amount'])?$results[0]['opportunity_amount']:'0';
				$amount=floatval(preg_replace('/[^\d.]/', '', $amount));
				$contact_fname = isset($results[0]['first_name'])?$results[0]['first_name']:'0';
				$contact_id = isset($results[0]['id'])?$results[0]['id']:'0';
				$contact_lname = isset($results[0]['last_name'])?$results[0]['last_name']:'0';
				$contact_email = isset($results[0]['email1'])?$results[0]['email1']:'0';
				$primary_address_street = isset($results[0]['primary_address_street'])?$results[0]['primary_address_street']:'0';
				$primary_address_city = isset($results[0]['primary_address_city'])?$results[0]['primary_address_city']:'0';
				$primary_address_state = isset($results[0]['primary_address_state'])?$results[0]['primary_address_state']:'0';
				$primary_address_postalcode = isset($results[0]['primary_address_postalcode'])?$results[0]['primary_address_postalcode']:'0';
				$primary_address_country = isset($results[0]['primary_address_country'])?$results[0]['primary_address_country']:'0';
				$phone_mobile = isset($results[0]['phone_mobile'])?$results[0]['phone_mobile']:'0';
				$phone_work = isset($results[0]['phone_work'])?$results[0]['phone_work']:'0';
				$phone_home = isset($results[0]['phone_home'])?$results[0]['phone_home']:'0';
				$description = isset($results[0]['description'])?$results[0]['description']:'0';
				$name_correction = isset($results[0]['name_correction_c'])?$results[0]['name_correction_c']:'0';
				$status = isset($results[0]['status'])?$results[0]['status']:'';
				
				$source_language = isset($results[0]['sprj1_source_lang_c'])?$results[0]['sprj1_source_lang_c']:'';
				$target_lang = isset($results[0]['sprj1_target_lang_c'])?$results[0]['sprj1_target_lang_c']:'';
				$service = isset($results[0]['sprj1_service_c'])?$results[0]['sprj1_service_c']:'';
				$service_type= isset($results[0]['sprj1_service_type_c'])?$results[0]['sprj1_service_type_c']:'';
				$approval_payment_dd_c= isset($results[0]['approval_payment_dd_c'])?$results[0]['approval_payment_dd_c']:'';
				$related_invoice_number= isset($results[0]['sprj1_relatedinvoice_no_c'])?$results[0]['sprj1_relatedinvoice_no_c']:'';
				$opportunity_name= $contact_fname.' '.$contact_lname;
				
				
				
			
				return array("email"=>$contact_email,"contact_fname"=>$contact_fname,"contact_lname"=>$contact_lname,"amount"=>$amount,"primary_address_street"=>$primary_address_street,"primary_address_city"=>$primary_address_city,"primary_address_state"=>$primary_address_state,"primary_address_postalcode"=>$primary_address_postalcode,"primary_address_country"=>$primary_address_country,"phone_mobile"=>$phone_mobile,"phone_work"=>$phone_work,"phone_home"=>$phone_home,'contact_id'=>$contact_id,'description'=>$description,'name_correction'=>$name_correction,'status'=>$status,'source_language'=>$source_language,'target_language'=>$target_lang,'service'=>$service,'service_type'=>$service_type,'approval_payment_dd_c'=>$approval_payment_dd_c,'opportunity_name'=>$opportunity_name,'related_invoice_number'=>$related_invoice_number);
			}else{
				return array();
			}
	}
	
	
	//making array to send to CRM
	function getUserDetailByEmail($emailAddress=false){
		if($emailAddress==false) return array();
		
		$CI =& get_instance();
		$CI->load->library('SugarCRM');
		$sugar= new Rest;
		$sugar->setUrl(getConfigItem('crm_url'));
		$sugar->setUsername(getConfigItem('crm_username'));
		$sugar->setPassword(getConfigItem('crm_password'));
		$sugar->connect();
		$error = $sugar->get_error();
		//checking for the error if coming
		if($error !== FALSE) {
			print_r($error);
		}
			
		//Global variables declared
		$url = getConfigItem('crm_url');
		$username = getConfigItem('crm_username');
		$password = getConfigItem('crm_password');
		/* $userid = $_REQUEST['id'];
		$paym_method = $_REQUEST['payment_method']; // credit_card, paypal, cheque, other
		$paym_status = $_REQUEST['payment_status']; // yes , no
		$inv_num = $_REQUEST['invoice_number']; */
		
		$login_parameters = array(
			"user_auth" => array(
				"user_name" => $username,
				"password" => md5($password),
			),
		);
		$login_result = call("login", $login_parameters, $url);
		$session_id = $login_result->id;
		$responseData=array();
		$search_by_module_parameters = array(
			"session" => $session_id,
			'search_string' => $emailAddress,
			'modules' => array(
				'Leads',
			),
			'offset' => 0,
			'max_results' => 5,
			'id' => '',
			'select_fields' => array(
				'id',
				'opportunity_amount',
				'account_name',
				'first_name',
				'last_name',
				'status',
				'approval_payment_dd_c'
			),
			'unified_search_only' => false,
			'favorites' => false
		);
		$search_by_module_result = call('search_by_module', $search_by_module_parameters, $url);
		//print_r($search_by_module_result);die;
		if(isset($search_by_module_result->entry_list[0]->records)){
			foreach($search_by_module_result->entry_list[0]->records as $record){
				$first_name=isset($record->first_name->value)?$record->first_name->value:'';
				$last_name=isset($record->last_name->value)?$record->last_name->value:'';
				$status=isset($record->status->value)?$record->status->value:'';
				$approval_payment_dd_c=isset($record->approval_payment_dd_c->value)?$record->approval_payment_dd_c->value:'';
				$amount=isset($record->opportunity_amount->value)?$record->opportunity_amount->value:'';
				$amount=floatval(preg_replace('/[^\d.]/', '', $amount));
				$responseData[]=array('id'=>isset($record->id->value)?$record->id->value:'',
									  'amount'=>$amount,
									  'name'=>$first_name.' '.$last_name,
									  'status'=>$status,
									  'approval_payment_dd_c'=>$approval_payment_dd_c,
									  'type'=>'ld');
			}
		
		}
		//if(empty($responseData)){
			$search_by_module_parameters = array(
				"session" => $session_id,
				'search_string' => $emailAddress,
				'modules' => array(
					'Accounts',
					'Contacts',
				),
				'offset' => 0,
				'max_results' => 2,
				'id' => '',
				'select_fields' => array(
					'id',
					'first_name',
				),
				'unified_search_only' => false,
				'favorites' => false
			);
			$search_by_module_result = call('search_by_module', $search_by_module_parameters, $url);
			//=========ENDGetExistingEmail==========//
			$contactRecords = $search_by_module_result->entry_list;//[1]->records
			//echo '<pre>';
			//print_r($contacts);die;
			$response=array();
			foreach($contactRecords as $contact){
			    $contacts=$contact->records; 
				if(isset($contacts[0]->id) && ! empty($contacts[0]->id)){
					$contact_id = $contacts[0]->id->value;
					$get_relationships_parameters = array(
						 'session'=>$session_id,
						 'module_name' => 'Contacts',
						 'module_id' => $contact_id,
						 'link_field_name' => 'opportunities',
						 'related_module_query' => '',
						 'related_fields' => array(
							'id',
							'amount',
							'name'
						 ),
						 'related_module_link_name_to_fields_array' => array(
						 ),
						 'deleted'=> '0',
						 'order_by' => '',
					);
				
					$get_relationships_result = call("get_relationships", $get_relationships_parameters, $url);
					$idResponses=$get_relationships_result->entry_list;
					foreach($idResponses as $idResponse){
						$amount=isset($idResponse->name_value_list->amount->value)?$idResponse->name_value_list->amount->value:'';
						$amount=floatval(preg_replace('/[^\d.]/', '', $amount));
						$opportunity = $sugar->get_by_id($idResponse->name_value_list->id->value,'Opportunities',array('sales_stage'));
						$responseData[]=array('id'=>isset($idResponse->name_value_list->id->value)?$idResponse->name_value_list->id->value:'',
											 'amount'=>$amount,
											 'name'=>isset($idResponse->name_value_list->name->value)?$idResponse->name_value_list->name->value:'',
											 'type'=>'op',
											 'sales_stage'=>isset($opportunity[0]['sales_stage'])?$opportunity[0]['sales_stage']:'',
											 'approval_payment_dd_c'=>isset($opportunity[0]['approval_payment_dd_c'])?$opportunity[0]['approval_payment_dd_c']:'',
											 'description'=>isset($opportunity[0]['description'])?$opportunity[0]['description']:'',
											 'name_correction'=>isset($opportunity[0]['name_correction_c'])?$opportunity[0]['name_correction_c']:'');
					}
				}
			}
		//}
		return $responseData;
}
	//function to send request to crm and it will return the result to the function
	function call($method, $parameters, $url){
		ob_start();
		$curl_request = curl_init();
		curl_setopt($curl_request, CURLOPT_URL, $url);
		curl_setopt($curl_request, CURLOPT_POST, 1);
		curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
		curl_setopt($curl_request, CURLOPT_HEADER, 1);
		curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
	
		$jsonEncodedData = json_encode($parameters);
	
		$post = array(
			 "method" => $method,
			 "input_type" => "JSON",
			 "response_type" => "JSON",
			 "rest_data" => $jsonEncodedData
		);
	
		curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
		$result = curl_exec($curl_request);
		curl_close($curl_request);
	
		$result = explode("\r\n\r\n", $result, 2);
		$response = json_decode($result[1]);
		return $response;
	}
	
	
	
	function Userdetails($userid){
		
	
		$CI =& get_instance();
		$CI->load->library('SugarCRM');
		$sugar= new Rest;
		$sugar->setUrl(getConfigItem('crm_url'));
		$sugar->setUsername(getConfigItem('crm_username'));
		$sugar->setPassword(getConfigItem('crm_password'));
		$sugar->connect();
		$error = $sugar->get_error();
		//checking for the error if coming
		if($error !== FALSE) {
			print_r($error);
		}
	
		//Global variables declared
		$url = getConfigItem('crm_url');
		$username = getConfigItem('crm_username');
		$password = getConfigItem('crm_password');
		$paym_method = isset($_REQUEST['payment_method'])?$_REQUEST['payment_method']:''; // credit_card, paypal, cheque, other
		$paym_status = isset($_REQUEST['payment_status'])?$_REQUEST['payment_status']:''; // yes , no
		$inv_num = isset($_REQUEST['invoice_number'])?$_REQUEST['invoice_number']:'';
		
		//End of Global Variable Declaration//
		
		//=========LOGIN==========//
		$login_parameters = array(
			"user_auth" => array(
				"user_name" => $username,
				"password" => md5($password),
			),
		);
		$login_result = call("login", $login_parameters, $url);
		$session_id = $login_result->id;
		//=========END-LOGIN==========//
		if(isset($userid) && ! empty($userid)){
			$get_entries_parameters = array(
				 'session' => $session_id,
				 'module_name' => 'Opportunities',
				 'ids' => array(
					 $userid
				 ),
				 'select_fields' => array(
					'amount',
					'name',
					//'description',
				 ),
				 'link_name_to_fields_array' => array(
					array(
					 'name' => 'contacts',
					 'value' => array(
						 'id',
						 'first_name',
						 'last_name',
						 'email1',
						 'primary_address_street',
						 'primary_address_city',
						 'primary_address_state',
						 'primary_address_postalcode',
						 'primary_address_country',
						 'phone_mobile',
						 'phone_work',
						 'phone_home',
					  ),
					),
				 ),
			);
			$get_entries_result = call('get_entries', $get_entries_parameters, $url);
			if(isset($get_entries_result->relationship_list[0]->link_list[0]->records) && !empty($get_entries_result->relationship_list[0]->link_list[0]->records)){
				$opportunity = $sugar->get_by_id($userid,'Opportunities',array('sales_stage'));
				//print_r($opportunity);die;
				$amount = $get_entries_result->entry_list[0]->name_value_list->amount->value;
				$amount=floatval(preg_replace('/[^\d.]/', '', $amount));
				$contact_fname = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->first_name->value;
				$contact_id = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->id->value;
				$contact_lname = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->last_name->value;
				$contact_email = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->email1->value;
				$primary_address_street = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->primary_address_street->value;
				$primary_address_city = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->primary_address_city->value;
				$primary_address_state = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->primary_address_state->value;
				$primary_address_postalcode = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->primary_address_postalcode->value;
				$primary_address_country = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->primary_address_country->value;
				$phone_mobile = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->phone_mobile->value;
				$phone_work = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->phone_work->value;
				$phone_home = $get_entries_result->relationship_list[0]->link_list[0]->records[0]->link_value->phone_home->value;
				
				$source_language = isset($opportunity[0]['sprj1_source_lang_c'])?$opportunity[0]['sprj1_source_lang_c']:'';
				$target_lang = isset($opportunity[0]['sprj1_target_lang_c'])?$opportunity[0]['sprj1_target_lang_c']:'';
				$service = isset($opportunity[0]['sprj1_service_c'])?$opportunity[0]['sprj1_service_c']:'';
				$service_type= isset($opportunity[0]['sprj1_service_type_c'])?$opportunity[0]['sprj1_service_type_c']:'';
				$approval_payment_dd_c= isset($opportunity[0]['approval_payment_dd_c'])?$opportunity[0]['approval_payment_dd_c']:'';
				$opportunity_name= isset($opportunity[0]['name'])?$opportunity[0]['name']:'';
				$opportunity_id= isset($opportunity[0]['id'])?$opportunity[0]['id']:'';
				
			
				return array("email"=>$contact_email,"contact_fname"=>$contact_fname,"contact_lname"=>$contact_lname,"amount"=>$amount,"primary_address_street"=>$primary_address_street,"primary_address_city"=>$primary_address_city,"primary_address_state"=>$primary_address_state,"primary_address_postalcode"=>$primary_address_postalcode,"primary_address_country"=>$primary_address_country,"phone_mobile"=>$phone_mobile,"phone_work"=>$phone_work,"phone_home"=>$phone_home,'contact_id'=>$contact_id,'sales_stage'=>isset($opportunity[0]['sales_stage'])?$opportunity[0]['sales_stage']:'','description'=>isset($opportunity[0]['description'])?$opportunity[0]['description']:'','name_correction'=>isset($opportunity[0]['name_correction_c'])?$opportunity[0]['name_correction_c']:'','related_invoice_number'=>isset($opportunity[0]['sprj1_relatedinvoice_no_c'])?$opportunity[0]['sprj1_relatedinvoice_no_c']:'','certification_cost'=>isset($opportunity[0]['sprj1_cost_certification_c'])?$opportunity[0]['sprj1_cost_certification_c']:'','notarization_cost'=>isset($opportunity[0]['sprj1_cost_notarization_c'])?$opportunity[0]['sprj1_cost_notarization_c']:'','source_language'=>$source_language,'target_language'=>$target_lang,'service'=>$service,'service_type'=>$service_type,'approval_payment_dd_c'=>$approval_payment_dd_c,'opportunity_name'=>$opportunity_name,'opportunity_id'=>$opportunity_id);
			}else{
				return array();
			}
			
			// $set_entry_parameters = array(
		 //        "session" => $session_id,
			// 	"module_name" => "Opportunities",
		 //         "name_value_list" => array(
		 //              array("name" => "id", "value" => $userid),
		 //              array("name" => "payment_method_c", "value" => $paym_method),
		 //              array("name" => "payment_status_c", "value" => $paym_status),
		 //              array("name" => "sprj1_invoice_no_c", "value" => $inv_num),
		 //         ),
		 //    );
		
		   // $set_entry_result = call("set_entry", $set_entry_parameters, $url);
		}
		
}

		function get_IP_address()
		{
			foreach (array('HTTP_CLIENT_IP',
						   'HTTP_X_FORWARDED_FOR',
						   'HTTP_X_FORWARDED',
						   'HTTP_X_CLUSTER_CLIENT_IP',
						   'HTTP_FORWARDED_FOR',
						   'HTTP_FORWARDED',
						   'REMOTE_ADDR') as $key){
				if (array_key_exists($key, $_SERVER) === true){
					foreach (explode(',', $_SERVER[$key]) as $IPaddress){
						$IPaddress = trim($IPaddress); // Just to be safe
		
						if (filter_var($IPaddress,
									   FILTER_VALIDATE_IP,
									   FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)
							!== false) {
		
							return $IPaddress;
						}
					}
				}
			}
		}
		
		//function to get contact detail
		function getContactDetail($contact_id=false){
			
			if($contact_id==false) return array();
			//Global variables declared
			$url = getConfigItem('crm_url');
			$username = getConfigItem('crm_username');
			$password = getConfigItem('crm_password');
			$paym_method = isset($_REQUEST['payment_method'])?$_REQUEST['payment_method']:''; // credit_card, paypal, cheque, other
			$paym_status = isset($_REQUEST['payment_status'])?$_REQUEST['payment_status']:''; // yes , no
			$inv_num = isset($_REQUEST['invoice_number'])?$_REQUEST['invoice_number']:'';
			
			//End of Global Variable Declaration//
			
			//=========LOGIN==========//
			$login_parameters = array(
				"user_auth" => array(
					"user_name" => $username,
					"password" => md5($password),
				),
			);
			$login_result = call("login", $login_parameters, $url);
			$session_id = $login_result->id;
			$get_relationships_parameters = array(
				 'session'=>$session_id,
				 'module_name' => 'Contacts',
				 'module_id' => $contact_id,
				 'link_field_name' => 'opportunities',
				 'related_module_query' => '',
				 'related_fields' => array(
					'id',
					'amount',
					'name'
				 ),
				 'related_module_link_name_to_fields_array' => array(
				 ),
				 'deleted'=> '0',
				 'order_by' => '',
			);
		
			$get_relationships_result = call("get_relationships", $get_relationships_parameters, $url);
			$response=$get_relationships_result->entry_list;
			return $response;
		}

