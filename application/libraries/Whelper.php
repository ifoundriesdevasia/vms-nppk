<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WHelper {
	protected static $_cache = array();

	/**
	 * @var CI_Controller
	 */
	protected static $_ci;

	/**
	 *
	 * @param string $ns A cache container
	 * @param mixed $dft Default value for cache container. Default is array.
	 * @return array
	 */
	public static function &getCacheContainer($ns = 'default', $dft = array()) {
		$caches = &self::$_cache;
		if(!isset($caches[$ns])) {
			$caches[$ns] = $dft;
		}
		return $caches[$ns];
	}

	public static function debug($var = NULL, $heading = FALSE, $simple = FALSE, $display = TRUE) {
		$msg = array();
		$msg[] = ($display === false) ? "<pre class='debug-info' style='display:none; padding: 12px 0;'>" : "<pre class='debug-info' style='padding: 12px 0;'>";

		// trace
		$heading2 = '';
		if (!$simple && is_callable('debug_backtrace')) {
			$trace = debug_backtrace();
			if(!empty($trace[1])) {
				$t = $trace[0];
				$t2 = $trace[1];

				if (isset($t2['class'])) {
					$heading2 .= "{$t2['class']}";

					if (isset($t2['function'])) {
						$heading2 .= "::{$t2['function']}()";
					}
				}
				else {
					if (isset($t2['function'])) {
						if(in_array($t2['function'], array('include', 'include_once', 'require', 'require_once'))) {
							$heading2 .= "{$t2['function']}(". substr($t['file'], strlen(JPATH_SITE)).")";
						}
						else {
							$heading2 .= "{$t2['function']}()";
						}
					}
				}

				if (isset($t['line'])) {
					$heading2 .= " line {$t['line']}";
				}
			}
		}

		$msg[] = $heading ? "<strong>{$heading}</strong>: {$heading2}\n" : "{$heading2}\n";

		$msg[] = print_r($var, TRUE);
		$msg[] = "\n</pre>\n";
		$msg = implode('', $msg);
		if ($display === 2) {
			return $msg;
		}
		echo $msg;
	}

	public static function debugTraces($deep=false, $heading = false, $return = false, $display = true) {
		$traces = debug_backtrace();

		$str = '';
		foreach($traces as $trace) {
			if(!$deep) {
//				unset($trace['object'], $trace['args']);
				unset($trace['object']);
			}

			$str .= print_r($trace, 1);
			$str .= "\n";
		}

		return self::debug($str, $heading, $return, $display);
	}

	public static function addLogs($fileName, $logInf, $logType = 'error') {
		//making log message
		$str = "[" . date("D M d H:i:s Y") . "] [" . $logType . "] [client " . getenv("REMOTE_ADDR") . "] ";
		//appending || writing
		$fmode = (file_exists($fileName)) ? 'a' : 'w';
		//opening log file
		$fp = @fopen($fileName, $fmode);
		//writing message to log file
		@fputs($fp, $str . $logInf . "\r\n");
		//closing log file
		fclose($fp);
	}

	public static function addLog($logInf, $logType = 'log', $fileName=null) {
		if(is_null($fileName)) {
			$fileName = 'whelper.log';
		}

		$tmpDir = rtrim(JFactory::getConfig()->get('log_path', ''), '\\/');
		$fpath = $tmpDir . '/' . $fileName;

		$prev = array();
		$pprev = array();
		if (is_callable('debug_backtrace')) {
			$trace = debug_backtrace();
			if (count($trace) > 1) {
				$prev = $trace[0];

				if(isset($trace[1])) {
					$pprev = $trace[1];
				}
			}
		}

		//making log message
		$str = "=======================================================\n";

		$str .= "[" . date("D M d H:i:s Y") . "]";

		if (isset($prev['file'])) {
			$str .= "[{$prev['file']}]";
		}

//		if (isset($prev['function'])) {
//			$str .= "[{$prev['function']}]";
//		}
		if (isset($pprev['function'])) {
			$str .= "[{$pprev['function']}]";
		}

		if (isset($prev['line'])) {
			$str .= "[line {$prev['line']}]";
		}

		if(!empty($logType)) {
			if(!is_array($logType)) {
				$logType = (array) $logType;
			}

			foreach($logType as $type) {
				$str .= "[{$type}]";
			}
		}

		$str .= "\n" .print_r($logInf, 1). "\n\n";

		//appending || writing
		$fmode = (file_exists($fpath)) ? 'a' : 'w';
		//opening log file
		$fp = @fopen($fpath, $fmode);
		//writing message to log file
		@fputs($fp, $str);
		//closing log file
		fclose($fp);
	}

	public static function getRandValue($len = 1, $val = '1234567890abcdefghijklmn0pqrstuvwxyz') {
		//1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
		//23456789abcdefghkmnpqrstwxyzABCDEFGHKMNPQRSTWXYZ
		//23456789abcdefghkmnpqrstwxyz
		$str = '';
		$len = (int) $len;
		if ($len <= 0) {
			return $str;
		}

		if (!$val) {
			$val = '1234567890abcdefghijklmnopqrstuvwxyz';
		}

		$valLen = strlen($val) - 1;
		for ($i = 0; $i < $len; $i++) {
			$str .= $val[rand(0, $valLen)];
		}
		return $str;
	}




	/**** CI Helper functions ****/
	public static function getCI() {
		if(!isset(self::$_ci)) {
			self::$_ci =& get_instance();
		}

		return self::$_ci;
	}

	public static function getCI_Input() {
		return self::getCI()->input;
	}

	public static function getUserIP() {
		return self::getCI_Input()->ip_address();
	}

	/**
	 * Get user.
	 * If ID is omitted then current session user will be returned.
	 * @param int $id
	 * @param bool $refresh
	 * @return object
	 */
	public static function getUser($id=null, $refresh=false) {
		$cache =& self::getCacheContainer('users');
		$id = (is_numeric($id) && $id > 0)? (int) $id : 'self';

		if(!isset($cache[$id]) || $refresh) {
			$ci = self::getCI();

			if('self' === $id) {
				$user = $ci->session->userdata('logged_in');
				if(empty($user)) {
					return false;
				}
				else {
					// ok, i know it is confusing
					// This is to set reference of variable to cache with numeric index
					$cache[$id] = (object) $user;
					$user =& $cache[$id];
					$id = $user->id;
				}
			}
			else {
				// load user model
				$ci->load->model('user', '', true);
				$user = $ci->user->getUser($id);
			}

			// remove password
			if($user && property_exists($user, 'password')) {
				unset($user->password);
			}

			$cache[$id] = $user; // numeric key
		}

		return $cache[$id];
	}

	/**
	 * Get pagination html
	 * @param string $url
	 * @param int $total
	 * @param int $perpage
	 * @param int $urlSegmentCnt Specify which segment of URI contains the page number
	 * @param array $config
	 * @return string
	 */
	public static function getPaginationHtml($url, $total, $perpage=20, $urlSegmentCnt=3, $config=array()) {
		$ci = self::getCI();
		$ci->load->library('pagination');

		$config['base_url'] = $url;
		$config['total_rows'] = $total;
		$config['per_page'] = $perpage;
		$config['uri_segment'] = $urlSegmentCnt;

		// base config
		$bconf = array(
			'full_tag_open'	=> '<ul class="pagination table-bootgrid__pagination">',
			'full_tag_close'	=> '</ul>',

			'num_tag_open'	=> '<li>',
			'num_tag_close'	=> '</li>',

			'cur_tag_open'	=> '<li class="disabled"><a href="#current">',
			'cur_tag_close'	=> '</a></li>',

			'first_tag_open'	=> '<li class="first" aria-disabled="false">',
			'first_tag_close'	=> '</li>',

			'last_tag_open'	=> '<li class="last" aria-disabled="false">',
			'last_tag_close'	=> '</li>',

			'prev_tag_open'	=> '<li class="prev" aria-disabled="true">',
			'prev_tag_close'	=> '</li>',

			'next_tag_open'	=> '<li class="next" aria-disabled="false">',
			'next_tag_close'	=> '</li>',
		);

		// override config
		if(!empty($config)) {
			foreach($config as $k => $v) {
				$bconf[$k] = $v;
			}
		}

		$ci->pagination->initialize($bconf);
		return $ci->pagination->create_links();
	}

	/**
	 * Method to bind an associative array or object to the target.
	 * This method only binds properties that are publicly accessible and
	 * optionally takes an array of properties to ignore when binding.
	 *
	 * @param   array|object  $target     An associative array or object to be bound with src.
	 * @param   array|object  $src     An associative array or object to bind to the target.
	 * @param   array|string  $ignore  An optional array or space separated list of properties to ignore while binding.
	 *
	 * @return  array|object  $target
	 */
	public static function objectBindData($target, $src, $ignore = array()) {
		// If the source value is not an array or object then cast it to array
		if (!is_object($src) && !is_array($src)) {
			$src = (array) $src;
		}
		// If the source value is an object, get its accessible properties.
		elseif (is_object($src)) {
			$src = get_object_vars($src);
		}

		// If the ignore value is a string, explode it over spaces.
		if (!is_array($ignore)) {
			$ignore = explode(' ', $ignore);
		}

		//
		$isObj = is_object($target);
		$targetSrc = $isObj ? get_object_vars($target) : $target;

		// Bind the source value, excluding the ignored fields.
		foreach ($targetSrc as $k => $v) {
			// Only process fields not in the ignore array.
			if (in_array($k, $ignore)) {
				continue;
			}

			if (isset($src[$k])) {
				$isObj ? $target->$k = $src[$k] : $target[$k] = $src[$k];
			}
		}

		return $target;
	}

	/**
	 * Hit or miss by giving the factor in absolute value of integer.
	 * Say the factor is 5, the chances to get a hit is 1/5.
	 * The higher factor the less chances it get hit.
	 *
	 * @param int $factor
	 * @return bool
	 */
	public static function hitOrMiss($factor) {
		$factor = (int) $factor;
		return ($factor > 0 && (1 === $factor || mt_rand(1, $factor) === 1));
	}

	/**
	 * Remove files if older than specific time.
	 *
	 * @param string $path	Full directory.
	 * @param bool $recursive	Whether to seek for sub-directories.
	 * @param int $maxDelete	Max file to be removed.
	 * @param mixed $oldTime	Remove file only if file older than its value. Time in seconds such as 1 day (86400) or relatie format such as "-1 day"
	 * @return void
	 */
	public static function cleanDirectory($path, $recursive=false, $maxDelete=300, $oldTime=86400) {
		// get cache
		$cache = self::getCacheContainer(__FUNCTION__);
		if(!isset($cache['counter'])) {
			$cache['counter'] = 0;
		}

		// stop if hit max
		if($cache['counter'] >= $maxDelete) {
			return;
		}

		if(!is_dir($path)) {
			return;
		}

		// settings
		$path = rtrim($path, '/\\');
		$curTime = time();

		// validate oldtime
		if(!is_numeric($curTime)) {
			$oldTime = @strtotime($oldTime);
			if(-1 === $oldTime || false === $oldTime) {
				$oldTime = 86400; // set 1 day if fail to parse
			}
			else {
				// get diff in seconds from now
				// expect value in +ve
				// -ve value for future time
				$oldTime = $curTime - $oldTime;
			}
		}

		// delete file more than 1 day
		$dirs = array();
		if ($handle = @opendir($path)) {
			$skipFiles = array(
				'.', '..',
				'index.html', 'index.php', '.htaccess',
			);

			while (false !== ($file = readdir($handle))) {
				// ignore
				if(in_array($file, $skipFiles)) {
					continue;
				}

				// full path
				$fpath = $path .'/'. $file;

				// store dir
				if(is_dir($fpath)) {
					$dirs[] = $fpath;
				}

				// remove file if older than specific time
				$modTime = filemtime($fpath);
				if(false === $modTime || $oldTime > ($curTime - $modTime)) {
					continue;
				}

				// delete
				unlink($fpath);

				// counter
				$cache['counter']++;

				// stop if hit max
				if($cache['counter'] >= $maxDelete) {
					break;
				}
			}

			closedir($handle);
		}

		// stop if hit max
		if($cache['counter'] >= $maxDelete) {
			return;
		}

		// iterate sub-directories
		if($recursive || empty($dirs)) {
			return;
		}

		foreach($dirs as $dir) {
			// stop if hit max
			if($cache['counter'] >= $maxDelete) {
				return;
			}

			self::cleanDirectory($dir, $recursive, $maxDelete, $oldTime);
		}
	}
	/**** EO CI Helper Functions ****/

}
