<?php
//Group_user model
class Group_user extends CI_Model {
	protected $table = '';
	protected $table_groups = 'groups';
	protected $table_group_users = 'group_users';
	protected $table_gx_users = 'gx_users';

	public function __construct() {
		parent::__construct();

		// load model
		$this->load->model('group', '', true);
		$this->load->model('gx_user_model', '', true);

		//$this->mssql = $this->load->database ( 'MSSQL', TRUE );
		$this->db = $this->load->database('default', TRUE);

		$this->table = $this->table_group_users;
	}

	public function exists($uid, $gid) {
		$row = $this->getGroupUser($uid, $gid);
		return ($row && !empty($row->user_id))? true : false;
	}

	public function existsNotGroup($uid, $gid) {
		$db = $this->db;
		$db->select('user_id')
			->from($this->table . ' AS a')
			->where('group_id <>', (int) $gid)
			->where('user_id', (int) $uid)
			->limit(1);

		$res = $db->get('');
		$row = $res->row();
		return empty($row->user_id)? false : true;
	}

	/**
	 * Remove assigned users from group
	 * @param int $gid
	 * @param int $ids
	 * @return array
	 */
	public function dissociateGroup($gid, $ids) {
		if(!is_array($ids)) {
			$ids = array($ids);
		}

		$ids = array_unique($ids);

		$db = $this->db;
		$counter = array(
			'total' => count($ids),
			'success' => 0,
			'fail' => 0,
		);

		// get group
		$groupData = $this->group->getData($gid);
		if(empty($groupData)) {
			return $counter;
		}

		// iterate group users
		foreach($ids as $uid) {
			$uid = (int) $uid;
			if($uid < 1) {
				$counter['fail']++;
				continue;
			}

			// check user exists in group?
//			if(!$this->exists($uid, $gid)) {
//				$counter['success']++;
//				continue;
//			}

			$grpUser = $this->getGroupUser($uid, $gid);
			if(!($grpUser && !empty($grpUser->user_id))) {
				$counter['success']++;
				continue;
			}

			// delete user from group
			$r = $db->where('group_id', (int) $gid)
				->where('user_id', (int) $uid)
				->delete($this->table);

			if($r) {
				$counter['success']++;

				// add action log
				$grpUser->group = $groupData[0];
				$grpUser->gxuser = $this->gx_user_model->getUser($uid);
				unset($grpUser->gxuser->xml);
				ActionLog::getInstance()->logGroupDissociateUser($grpUser->id, $grpUser);
			}
			else {
				$counter['fail']++;
			}
		}

		return $counter;
	}

	/**
	 * Assign users into group.
	 * @param int $gid
	 * @param int $ids
	 * @return array
	 */
	public function assignGroup($gid, $ids) {
		if(!is_array($ids)) {
			$ids = array($ids);
		}

		$ids = array_unique($ids);

		$counter = array(
			'total' => count($ids),
			'success' => 0,
			'fail' => 0,
			'other' => 0,
		);

		// get group
		$groupData = $this->group->getData($gid);
		if(empty($groupData)) {
			return $counter;
		}

		foreach($ids as $uid) {
			$uid = (int) $uid;
			if($uid < 1) {
				$counter['fail']++;
				continue;
			}

			// already in the group?
			if($this->exists($uid, $gid)) {
				$counter['success']++;
				continue;
			}

			// assigned in another group?
			if($this->existsNotGroup($uid, $gid)) {
				$counter['other']++;
				continue;
			}

			// get gx user
			$row = $this->gx_user_model->getUser($uid);

			// not exists?
			if(empty($row)) {
				$counter['fail']++;
				continue;
			}

			$data = array(
				'user_id' => $uid,
				'group_id' => $gid,
				'name' => $row->name,
//				'CardNumber' => $row->card_number,
//				'FamilyNumber' => $row->family_number,
				'has_tap' => 0,
//				'identifier' => $row->cf_visitor,
			);

			$r = $this->db->insert($this->table, $data);
			if($r) {
				$counter['success']++;

				unset($row->xml);

				$data['id'] = $this->db->insert_id();
				$data['group'] = $groupData[0];
				$data['gxuser'] = $row;

				// add action log
				ActionLog::getInstance()->logGroupAssignUser($data['id'], $data);
			}
			else {
				$counter['fail']++;
			}
		}

		return $counter;
	}

	public function get_data($gid, $params=array()) {
		$params['gid'] = $gid;

		$db = $this->getListQuery($params);

		$res = $db->select('g.id AS group_id, g.group_name, gu.*, gxu.*')
			->order_by('gxu.cf_visitor ASC') // escort first
			->get('');

		return $res->result();
	}

	public function deleteByGroupID($id) {
		$this->db->where('group_id', $id);
		return $this->db->delete($this->table);
	}

	/**
	 * @param int $id
	 * @param int $gid
	 * @return object
	 */
	public function getGroupUser($id, $gid) {
		if (empty($id) || empty($gid)) {
			return false;
		}

		$this->db->select('*')
			->from('group_users');

		$this->db->where('group_id', $gid)
			->where('user_id', $id);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}

		return false;
	}

	public function getCsvDataTotal($gid = NULL) {
		$params = array(
			'gid'	=> $gid,
		);

		$db = $this->getListQuery($params);

		$res = $db->select('COUNT(*) AS cnt')
			->get('');

		$row = $res->row();

		return empty($row->cnt)? 0 : $row->cnt;
	}

	public function getCsvData($gid = NULL, $limit=null, $offset=null) {
		$params = array(
			'gid'	=> $gid,
		);

		$db = $this->getListQuery($params);

		$res = $db->select('g.id AS group_id, g.group_name, gxu.*')
			->order_by('g.id ASC, gxu.cf_visitor ASC')
			->get('', $limit, $offset);

		return $res->result();
	}

	public function setUserTapStatus($gid, $uid, $status) {
		$data = array(
			'has_tap' => (bool) $status? 1 : 0,
		);
		$this->db->where('group_id', $gid)
			->where('user_id', $uid);
		$r = $this->db->update('group_users', $data);

		// update tap count
		$this->group->updateAllTapCount($gid);

		return $r;
	}

	public function getDataByCardNumber($cardNumber, $familyNumber) {
		$params = array(
			'where'	=> array(
				'gxu.card_number'	=> $cardNumber,
				'gxu.family_number'	=> $familyNumber,
			)
		);

		$row = $this->get_data('', $params);

		return $row? $row[0] : false;
	}

	public function getAllVisitors($gid, $tap=null) {
		if(!is_null($tap)) {
			$params['has_tap'] = (bool) $tap? 1 : 0;
		}

		$params['identifier'] = 1; // visitor

		return $this->get_data($gid, $params);
	}

	public function getAllEscorts($gid, $tap=null) {
		if(!is_null($tap)) {
			$params['has_tap'] = (bool) $tap? 1 : 0;
		}

		$params['identifier'] = 0; // visitor

		return $this->get_data($gid, $params);
	}

	public function resetUsersTap($gid) {
		if(empty($gid) || !is_numeric($gid)) {
			return false;
		}

		// valid group?
		$groupData = $this->group->getData($gid);
		if(empty($groupData)) {
			return false;
		}

		// add action log
		ActionLog::getInstance()
			->addExcludeEntry('group.edit')
			->logGroupResetTap($gid, $groupData[0]);

		$users = $this->group_user->getAllVisitors($gid);
		foreach ($users as $user) {
			$this->protegegx->disableUser($user->user_id, true);
		}

		//if (count($users) > 0) {
			$users = $this->group_user->getAllEscorts($gid, 1);
			if (count($users) > 0) {
				foreach ($users as $user) {
					$this->protegegx->disableUser($user->user_id, false);
				}
			}
		//}

		// reset tap status
		$data = array('has_tap' => 0);
		$r = $this->db->update($this->table_group_users, $data, array('group_id' => $gid));

		if(false === $r) {
			return $r;
		}

		// reset count
		return $this->group->updateAllTapCount($gid);
	}

	/**
	 * Get list query (basic)
	 * @param array $params Options for retrieving dataset
	 * @return CI_DB
	 */
	protected function getListQuery($params = array()) {
		$db = clone $this->db;

		//
		if(!is_array($params)) {
			$params = (array) $params;
		}

		if(empty($params['where'])) {
			$params['where'] = array();
		}

		// group ID
		if(isset($params['gid']) && '' != $params['gid']) {
			$params['where']['g.id'] = (int) $params['gid'];
		}

		// tap
		if(isset($params['has_tap'])) {
			$params['where']['gu.has_tap'] = (int) $params['has_tap'];
		}

		// identifier
		// 0: escort; 1: visitor
		if(isset($params['identifier'])) {
//			$db->where('gu.identifier', (int) $params['identifier']);
			$params['where']['gxu.cf_visitor'] = (int) $params['identifier'];
		}


		// query
		$db->from($this->table . ' AS gu')
			->join($this->table_groups . ' AS g', 'g.id = gu.group_id', 'INNER')
			->join($this->table_gx_users . ' AS gxu', 'gxu.user_id = gu.user_id', 'LEFT');

		// where clauses
		if(!empty($params['where'])) {
			foreach($params['where'] as $k => $v) {
				$db->where($k, $v);
			}
		}
		
// WHelper::debug($db->get_compiled_select('', false), 'query'); 

		return $db;
	}

}
