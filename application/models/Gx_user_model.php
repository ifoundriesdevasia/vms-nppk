<?php
class Gx_user_model extends CI_Model {
	/**
	 * @var CI_Controller
	 */
	protected $ci;

	protected $table_groups = 'groups';
	protected $table_group_users = 'group_users';
	protected $table_gx_users = 'gx_users';
	protected $table_gx_user_cards = 'gx_user_cards';

	protected $tableFields = array();
	protected $cfsData = array();

	public function __construct() {
		parent::__construct();

		// CI instance
		$this->ci =& get_instance();

		// load cache
		$this->ci->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
	}

	protected function getFields($table, $prefix='') {
		if(!isset($this->tableFields[$table])) {
			$this->tableFields[$table] = $this->db->list_fields('gx_users');
		}

		if('' !== $prefix) {
			$fields = array();
			foreach($this->tableFields[$table] as $fld) {
				$fields[] = "{$prefix}{$fld}";
			}

			return $fields;
		}

		return $this->tableFields[$table];
	}

	/**
	 * Get GX user by ID
	 * @param int $id
	 * @return object
	 */
	public function getUser($id) {
		$res = $this->db->select('*')
			->from($this->table_gx_users . ' AS a')
			->where('a.user_id', (int) $id)
			->get('');

		return $res->row();
	}

	/**
	 * Get total GX users
	 * @param array $params
	 * @return int
	 */
	public function getUsersTotal($params=array()) {
		$db = $this->getUsersQuery($params);

		$db->select('COUNT(*) AS cnt');

		$res = $db->get('');

		$row = $res->row();

		return empty($row->cnt)? 0 : $row->cnt;
	}

	/**
	 * Get GX users
	 * @param array $params
	 * @param int $limit
	 * @param int $offset
	 * @return array
	 */
	public function getUsers($params=array(), $limit=10, $offset=0) {
		$db = $this->getUsersQuery($params);

		if(!empty($params['dbfields'])) {
			$fields = $params['dbfields'];
		}
		else {
			$fields = $this->getFields($this->table_gx_users, 'a.');

//			if(!empty($params['dbfields_remove'])) {
//
//			}

			$k = array_search('a.xml', $fields);
			unset($fields[$k]);
		}

		$db->select($fields);

		$res = $db->get('', $limit, $offset);

		return $res->result();
	}

	/**
	 * Get total GX users with assigned group
	 * @param array $params
	 * @return int
	 */
	public function getUsersTotalWithGroup($params=array()) {
		return $this->getUsersTotal($params);
	}

	/**
	 * Get GX users with assigned group
	 * @param array $params
	 * @param int $limit
	 * @param int $offset
	 * @return array
	 */
	public function getUsersWithGroup($params=array(), $limit=10, $offset=0) {
		$db = $this->getUsersQuery($params);

		if(!empty($params['dbfields'])) {
			$fields = $params['dbfields'];
		}
		else {
			$fields = $this->getFields($this->table_gx_users, 'a.');

//			if(!empty($params['dbfields_remove'])) {
//
//			}

			$fields[] = 'g.group_name';
			$fields[] = 'gu.group_id';
			$fields[] = 'gu.has_tap';

			$k = array_search('a.xml', $fields);
			unset($fields[$k]);
		}

		$db->select($fields)
			->join("{$this->table_group_users} AS gu", 'gu.user_id = a.user_id', 'LEFT')
//			->join("{$this->table_groups} AS g", 'g.id = gu.group_id', 'INNER');
			->join("{$this->table_groups} AS g", 'g.id = gu.group_id', 'LEFT');

		$res = $db->get('', $limit, $offset);

		return $res->result();
	}

	public function getCfEntriesDesignation() {
		$this->fetchCustomFieldsData();
		return $this->cfsData['designation'];
	}

	public function getCfEntriesDepartment() {
		$this->fetchCustomFieldsData();
		return $this->cfsData['department'];
	}

	public function getCfEntriesDivision() {
		$this->fetchCustomFieldsData();
		return $this->cfsData['division'];
	}

	public function getCfEntriesTeam() {
		$this->fetchCustomFieldsData();
		return $this->cfsData['team'];
	}

	public function getCfEntriesCompany() {
		$this->fetchCustomFieldsData();
		return $this->cfsData['company'];
	}

	public function getCfEntriesVisitor() {
		$this->fetchCustomFieldsData();
		return $this->cfsData['visitor'];
	}

	protected function getCacheName() {
		$args = array_merge(func_get_args(), array(__FUNCTION__));
		return strtolower(__CLASS__) .'-'. md5(implode('.', $args));
	}

	protected function cleanCache() {
		$this->ci->cache->delete($this->getCacheName('CustomFieldsData'));
	}

	public function refresh_users_status($default=null) {
//		$sig = $this->getCacheName('refreshUsers');
//		$cache = $this->ci->cache->get($sig);
//		if(false === $cache) {
//			$cache['new'] = 1;
//		}

//		return $cache;

		$res = $this->db->select('*')
			->where('mkey', 'gxuser.refresh')
			->get('miscs');

		$row = $res->row();

		if(empty($row)) {
//			$data = array(
//				'mkey'	=> 'gxuser.refresh',
//				'mvalue'	=> '{}',
//			);
//
//			$this->db->insert('miscs', $data);

//			$r = new stdClass();
			$r = $default;
		}
		else {
			$r = empty($row->mvalue)? new stdClass() : json_decode($row->mvalue);
		}

		return $r;

	}

	public function set_refresh_users_status($data) {
		$old = $this->refresh_users_status();

		$data = array(
			'mkey'	=> 'gxuser.refresh',
			'mvalue'	=> json_encode($data),
		);

		// replace is not standard sql
//		$this->db->replace('miscs', $data);

		if(is_null($old)) {
			$this->db->insert('miscs', $data);
		}
		else {
			$this->db->where('mkey', $data['mkey'])
				->update('miscs', $data);
		}

		return $old;
	}

	public function refresh_users($debug=false) {
		set_time_limit(0); // no timeout

		$db = $this->db;

//		$sig = $this->getCacheName('refreshUsers');
//		$cache = $this->ci->cache->get($sig);
//		if(false === $cache) {
//			$cache['started'] = time();
//			$cache['process'] = 1;
//			$this->ci->cache->save($sig, $cache, 86400); // 1 day
//		}
//		// check data syncing in progress?
//		elseif(!empty($cache['process'])) {
//			return false;
//		}
////		// remove ended time when it is in progress
////		elseif(!empty($cache['ended'])) {
////			unset($cache['ended']);
////			$this->ci->cache->save($sig, $cache, 86400); // 1 day
////		}

		$cache = $this->refresh_users_status();
		if(is_null($cache)) {
			$cache = new stdClass();
			$cache->started = time();
			$cache->process = 1;
			$this->set_refresh_users_status($cache);
		}
		// check data syncing in progress?
		elseif(!empty($cache->process)) {
			return false;
		}

		// add action log
		ActionLog::getInstance()->logProtegegxRefreshUsers();


		$offset = -1;
		$perrun = 300;
		$insertLimit = 10; // batch insert limit
		$torelence = 5;
		$emptyCnt = 0;
		$readCnt = 0;
		$insertCnt = 0;
		$errors = array();
		$debugs = array();


		// empty tmp tables
		$db->truncate("{$this->table_gx_users}_tmp");
		$db->truncate("{$this->table_gx_user_cards}_tmp");

		while(true) {
			$offset = (-1 === $offset)? 0 : $offset + $perrun;

			// load user records
			try {
				$row = $this->protegegx->listRecordsUsers($offset, $perrun);
			}
			catch (Exception $ex) {
//				continue;
				$errors[] = 'Exception caught: ' . $ex->getMessage();
				break;
			}


			// store data for debug
			//if($debug) {
				$debugs[] = $row;
			//}

			// empty dataset
//			if (!isset($row->RData->RecordData->DeviceName)) {
			if (empty($row->RData->RecordData->DeviceName)) {
				break;
			}

			//
			$batchUsers = array();
			$batchCards = array();
			$rows = $row->RData->RecordData->DeviceName;
			foreach ($rows as $userrow) {
				try {
					$row = $this->protegegx->getRecordUser($userrow->ID);
				}
				catch (Exception $ex) {
					$errors[] = 'Exception caught: ' . $ex->getMessage();
					continue;
				}

				// counter - read
				$readCnt++;


				// empty?
				if(false === $row || empty($row->_xml)) {
					$emptyCnt++;
					if($emptyCnt >= $torelence) {
//						break 2; // assume no more entries

						// break here
						// do batch insert if any at outer loop
						break; // assume no more entries
					}

					continue; // record not found
				}

				// reset counter when there is data
				$emptyCnt = 0;


				// insert
				$batchUsers[] = array(
					'user_id' => $row->_data['UserID'],
					'name' => $row->_data['Name'],
					'card_number' => $row->_data['CardNumber'],
					'family_number' => $row->_data['FamilyNumber'],
					'card_disabled' => $row->_data['CardDisabled'],
					'user_disabled' => $row->_data['DisableUser'],
					'card_cnt' => count($row->_data['_cards']),

					'cf_visitor' => $row->_data['IsVisitor'],
					'cf_nric' => $row->_data['Nric'],
					'cf_designation' => $row->_data['Designation'],
					'cf_department' => $row->_data['Department'],
					'cf_division' => $row->_data['Division'],
					'cf_team' => $row->_data['Team'],
					'cf_off_phone' => $row->_data['OfficeNumber'],
					'cf_mobile' => $row->_data['MobileNumber'],
					'cf_email' => $row->_data['Email'],
					'cf_company' => $row->_data['Company'],
					'xml' => $row->strXML,
				);

				// counter - insert
				$insertCnt++;

				// insert card data
				if(empty($row->_data['_cards'])) {
					continue;
				}

				foreach($row->_data['_cards'] as $card) {
					$batchCards[] = array(
						'user_id' => $row->_data['UserID'],
						'card_num' => $card['CardNumber'],
						'family_num' => $card['FamilyNumber'],
						'disabled' => $card['Disabled'],
					);
				}
			}


			// batch insert
			if($batchUsers) {
				$db->insert_batch("{$this->table_gx_users}_tmp", $batchUsers, true, $insertLimit);
			}

			if($batchCards) {
				$db->insert_batch("{$this->table_gx_user_cards}_tmp", $batchCards, true, $insertLimit);
			}


			// stats
			$stats = array(
				'read'	=> $readCnt,
				'inserted'	=> $insertCnt,
				'errors'	=> $errors,
				'debugs'	=> $debugs,
			);


//			// save stats to cache - for ajax
//			// note: batch insert not yet exec
//			$cache['stats'] = $stats;
//			$this->ci->cache->save($sig, $cache, 86400); // 1 day
			$cache->stats = $stats;
			$this->set_refresh_users_status($cache);

			// break?
			if($emptyCnt >= $torelence) {
				break;
			}

			// rest
			sleep(10);
		}


		// empty old table
		$db->truncate($this->table_gx_users);
		$db->truncate($this->table_gx_user_cards);

		// copy data from tmp
		$query = "INSERT {$this->table_gx_users} SELECT * FROM {$this->table_gx_users}_tmp";
		$db->query($query);

		// copy data from tmp - cards
		$query = "INSERT {$this->table_gx_user_cards} SELECT * FROM {$this->table_gx_user_cards}_tmp";
		$db->query($query);


		// stats
		$stats = array(
			'read'	=> $readCnt,
			'inserted'	=> $insertCnt,
			'errors'	=> $errors,
			'debugs'	=> $debugs,
		);


//		// save status
//		$cache['ended'] = time();
//		$cache['process'] = 0;
//		$cache['stats'] = $stats;
//		$this->ci->cache->save($sig, $cache, 86400); // 1 day
		$cache->ended = time();
		$cache->process = 0;
		$cache->stats = $stats;
		$this->set_refresh_users_status($cache);

		// remove cache
		$this->cleanCache();

		// add action log
		ActionLog::getInstance()->logProtegegxRefreshUsersEnd($stats);

		return $stats;
	}

	protected function fetchCustomFieldsData() {
		$sig = $this->getCacheName('CustomFieldsData');

		if(false === ($cache = $this->ci->cache->get($sig))) {
			$assocCfs = array(
				'cf_designation'	=> 'designation',
				'cf_department'	=> 'department',
				'cf_division'	=> 'division',
				'cf_team'	=> 'team',
				'cf_company'	=> 'company',
			);

			foreach($assocCfs as $dbFld => $arrKey) {
				$res = $this->db->select("DISTINCT {$dbFld} AS val", false)
						->get($this->table_gx_users);
				$vals[$arrKey] = array();
				foreach ($res->result() as $row) {
					$row->val = trim('' . $row->val);
					if('' === $row->val) {
						continue;
					}

					$vals[$arrKey][$row->val] = $row->val;
				}
			}

			$vals['visitor'] = array(
				0	=> 'Escort',
				1	=> 'Visitor',
			);

			$cache = $vals;

			$this->ci->cache->save($sig, $cache, 604800); // 1 week
		}

		$this->cfsData = $cache;
	}

	/**
	 * Get query for GX Users
	 * @param array $params
	 * @return CI_DB
	 */
	protected function getUsersQuery($params=array()) {
		$db = clone $this->db;

		// reset query
		$db->reset_query();

//		$db->select("*");

		$db->from($this->table_gx_users . ' AS a');

		//
		if(!is_array($params)) {
			$params = (array) $params;
		}

		// search
		if(isset($params['search']) && '' != $params['search']) {
			$allowedFields = array(
				'name',
				'cf_nric',
				'card_number',
			);

			$scope = isset($params['search_scope'])? $params['search_scope'] : 'name';
			if(!in_array($scope, $allowedFields)) {
				$scope = 'name';
			}

			$search = "'%" . str_replace(' ', '%', $db->escape_str(trim($params['search']), true) . "%'");

			if('card_number' == $scope) {
				$db->join("{$this->table_gx_user_cards} AS c", 'c.user_id = a.user_id', 'INNER');

				$where = 'c.card_num LIKE ' . $search;
				$db->where($where);
			}
			else {
				$where = 'a.' . $scope . ' LIKE ' . $search;
				$db->where($where);
			}
		}


		// filter - designation
		if(isset($params['designation']) && '' != $params['designation']) {
			$db->where('a.cf_designation', $params['designation']);
		}


		// filter - department
		if(isset($params['department']) && '' != $params['department']) {
			$db->where('a.cf_department', $params['department']);
		}


		// filter - division
		if(isset($params['division']) && '' != $params['division']) {
			$db->where('a.cf_division', $params['division']);
		}


		// filter - team
		if(isset($params['team']) && '' != $params['team']) {
			$db->where('a.cf_team', $params['team']);
		}


		// filter - company
		if(isset($params['company']) && '' != $params['company']) {
			$db->where('a.cf_company', $params['company']);
		}


		// filter - visitor
		if(isset($params['visitor']) && '' != $params['visitor']) {
			$db->where('a.cf_visitor', (int) $params['visitor']);
		}

		return $db;
	}

}
