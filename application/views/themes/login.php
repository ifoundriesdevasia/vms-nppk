<?php
$siteName = $this->config->item('site_name');
$siteSubheader = $this->config->item('site_subheader');
$headbandColor = $this->config->item('headband_color');
$headbandTxtColor = $this->config->item('headband_text_color');

if($siteName) {
//	$this->output->set_title($siteName);
	$title = empty($title)? $siteName : "{$siteName} - {$title}";
}
?>
<!DOCTYPE html>
<html lang="en">
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title><?php echo $title; ?></title>

        <link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/themes/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/themes/css/app-1.min.css" rel="stylesheet" />

		<style>
			body:before {
				height: auto !important;
				padding: 2px 0;
				text-align: center;
				font-weight: bold;
				font-size: 18px;
				color: <?php echo $headbandTxtColor? $headbandTxtColor : '#fff'; ?>;
				background-color: <?php echo $headbandColor? $headbandColor : '#000'; ?>;
				z-index: 1;

				content: "<?php echo htmlspecialchars($siteName? $siteName : $siteSubheader); ?>";
			}
		</style>
    </head>

    <body>
        <div class="login">
            <!-- Login -->
            <div class="login__block toggled" id="l-login">
				<?php echo $output; ?>
            </div>
        </div>

        <script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!--[if IE 9 ]>
            <script src="<?php echo base_url(); ?>assets/themes/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url(); ?>assets/themes/js/app.min.js"></script>
    </body>
</html>
