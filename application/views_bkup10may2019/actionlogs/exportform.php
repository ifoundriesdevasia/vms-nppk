<?php
$colCnt = 4;
?>
<style>
	.adminform .form-group:not(.form-group--float) {
		margin-bottom: 12px;
	}

	.pagination {
		float: right;
		margin-top: 0;
	}
</style>

<script type="text/javascript">
	!function($){
		$(document).ready(function(){
			$('input.date-picker').datetimepicker({
                format: 'YYYY-MM-DD'
            });
		});
	}(jQuery);
</script>


<section id="content">
	<div class="card">
		<div class="card__header">
			<h2>Logs Report<small></small></h2>
		</div>


		<div class="card__body">
			<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('error')): ?>
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('warning')): ?>
				<div class="alert alert-warning">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
				</div>
			<?php endif; ?>

			<?php if ($this->session->flashdata('info')): ?>
				<div class="alert alert-info">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
			<?php endif; ?>


			<form name="adminform" class="adminform" action="<?php echo site_url('actionlogs/form'); ?>" method="post">
				<div class="row">
					<div class="col-sm-2">
						<p>Date Option :</p>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<?php echo $formhtml['dateoption']; ?>
							<i class="form-group__bar"></i>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2">
						<p>Date Start: </p>
					</div>

					<div class="col-sm-2">
						<div class="form-group">
							<?php echo $formhtml['date1']; ?>
							<i class="form-group__bar"></i>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2">
						<p>Date End: </p>
					</div>

					<div class="col-sm-2">
						<div class="form-group">
							<?php echo $formhtml['date2']; ?>
							<i class="form-group__bar"></i>
						</div>
					</div>
				</div>

				<input type="submit" value="Search" class="btn btn-default" />
			</form>
		</div>
	</div>




	<div class="card">
		<div class="card__body">
			<div class="table-responsive">
				<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Date</th>
							<th>User</th>
							<th>Action</th>
							<th>IP</th>
						</tr>
					</thead>

					<tbody>
						<?php if (empty($logs)): ?>
							<tr>
								<td colspan="<?php echo $colCnt; ?>"><h3 style="color:red;">Entry not found!</h3></td>
							</tr>

						<?php else: ?>
							<?php
							foreach ($logs as $log):
								$uname = $log->log_user_name;
								if('' != $log->u_name && $uname !== $log->u_name) {
									$uname .= " ($log->u_name)";
								}
							?>
								<tr>
									<td><?php echo $log->log_created; ?></td>
									<td><?php echo $uname; ?></td>
									<td><?php echo $log->action_msg; ?></td>
									<td><?php echo $log->log_ip; ?></td>
								</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>


					<?php if (!empty($logs)): ?>
					<tfoot>
						<tr>
							<td colspan="<?php echo $colCnt; ?>">
								<a href="<?php echo $logs_export_link; ?>"
									class="btn btn-default"
								>Export</a>

								<?php if (!empty($logs_pagelinks)): ?>
									<?php echo $logs_pagelinks; ?>
									<div class="clearfix"></div>
								<?php endif; ?>
							</td>
						</tr>
					</tfoot>
					<?php endif; ?>
				</table>
			</div>
		</div>
	</div>
</section>
