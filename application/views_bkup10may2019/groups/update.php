<section id="content">
    <div class="card">
        <div class="card__header">
            <h2>Update Group</h2>
        </div>

        <div class="card__body">
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
				</div>
			<?php } else if ($this->session->flashdata('error')) { ?>
				<div class="alert alert-danger">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
				</div>
			<?php } else if ($this->session->flashdata('warning')) { ?>
				<div class="alert alert-warning">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
				</div>
			<?php } else if ($this->session->flashdata('info')) { ?>
				<div class="alert alert-info">
					<a href="#" class="close" data-dismiss="alert">&times;</a>
					<strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
				</div>
			<?php } ?>


            <div class="row">
				<form id="group_add" action="<?php echo site_url('groups/update/' . $result['0']->id); ?>" method="post" >
					<input type="hidden" name="id" value="<?php echo $result['0']->id; ?>" />

                    <div class="input-group col-sm-12">
                        <span class="input-group-addon"><i class="zmdi zmdi-city-alt"></i></span>

                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Group Name" value="<?php echo $result['0']->group_name; ?>" />
							<i class="form-group__bar"></i>
                        </div>
                    </div>

					<br/>
                    <button type="submit" name="submit" class="btn btn-default">Update</button>&nbsp;&nbsp;
					<button type="button" name="delete" class="btn btn-default" onClick="return doconfirm('<?php echo site_url('groups/delete/' . $result['0']->id); ?>');">Delete</button>
				</form>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
	function doconfirm(url) {
		if (confirm("Do you want to delete this permanently?")) {
			window.location = url;
		}

		return false;
	}
</script>
