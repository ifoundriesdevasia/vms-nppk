<?php
$siteName = $this->config->item('site_name');
//$siteSubheader = $this->config->item('site_subheader');

//if($siteName) {
////	$this->output->set_title($siteName);
//	$title = empty($title)? $siteName : "{$siteName} - {$title}";
//}
?>
<?php if($this->session->flashdata('success')){ ?>
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php }else if($this->session->flashdata('error')){  ?>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php }else if($this->session->flashdata('warning')){  ?>
    <div class="alert alert-warning">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Warning!</strong> <?php echo $this->session->flashdata('warning'); ?>
    </div>
<?php }else if($this->session->flashdata('info')){  ?>
    <div class="alert alert-info">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Info!</strong> <?php echo $this->session->flashdata('info'); ?>
    </div>
<?php } ?>

<div class="login__block__header">
    <img src="<?php echo base_url(); ?>assets/themes/img/logo.png" alt="" />
	<?php if ('' !== $siteName): ?>
		<?php echo $siteName; ?>
	<?php endif; ?>
</div>

<form id="login_form" action="<?php echo site_url('users/login');?>" method="post">
    <div class="login__block__body">
        <div class="form-group form-group--float form-group--centered form-group--centered">
            <!--<input type="text" class="form-control">-->
            <input type="email" name="email" class="form-control" value="" tabindex="1" autofocus="true" />
            <label>Username</label>
            <i class="form-group__bar"></i>
        </div>

        <div class="form-group form-group--float form-group--centered form-group--centered">
            <input type="password" name="password" class="form-control" />
            <label>Password</label>
            <i class="form-group__bar"></i>
        </div>

        <button type="submit" class="btn btn--light btn--icon m-t-15"><i class="zmdi zmdi-long-arrow-right"></i></button>
    </div>
</form>