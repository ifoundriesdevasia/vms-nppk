-- alter group_users
ALTER TABLE `group_users` ADD INDEX group_id(`group_id`);
ALTER TABLE `group_users` ADD INDEX group_user_id( `group_id`, `user_id`);
ALTER TABLE `group_users` ADD `name` VARCHAR(255) NOT NULL AFTER `user_id`;
ALTER TABLE `group_users` CHANGE `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;

-- alter groups
ALTER TABLE `groups`
	ADD `visitor_in_count` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `group_name`,
	ADD `visitor_out_count` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `visitor_in_count`,
	ADD `escort_in_count` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `visitor_out_count`,
	ADD `escort_out_count` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `escort_in_count`;


--
-- Table structure for table `gx_users`
--

CREATE TABLE `gx_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `card_number` varchar(255) NOT NULL,
  `family_number` varchar(255) NOT NULL,
  `card_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `cf_visitor` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Escort; 1: Visitor',
  `cf_nric` varchar(255) NOT NULL,
  `cf_designation` varchar(255) NOT NULL,
  `cf_department` varchar(255) NOT NULL,
  `cf_division` varchar(255) NOT NULL,
  `cf_team` varchar(255) NOT NULL,
  `cf_off_phone` varchar(255) NOT NULL,
  `cf_mobile` varchar(255) NOT NULL,
  `cf_email` varchar(255) NOT NULL,
  `cf_company` varchar(255) NOT NULL,
  `user_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `card_cnt` int(10) NOT NULL DEFAULT '0',
  `xml` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gx_users_tmp`
--

CREATE TABLE `gx_users_tmp` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `card_number` varchar(255) NOT NULL,
  `family_number` varchar(255) NOT NULL,
  `card_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `cf_visitor` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Escort; 1: Visitor',
  `cf_nric` varchar(255) NOT NULL,
  `cf_designation` varchar(255) NOT NULL,
  `cf_department` varchar(255) NOT NULL,
  `cf_division` varchar(255) NOT NULL,
  `cf_team` varchar(255) NOT NULL,
  `cf_off_phone` varchar(255) NOT NULL,
  `cf_mobile` varchar(255) NOT NULL,
  `cf_email` varchar(255) NOT NULL,
  `cf_company` varchar(255) NOT NULL,
  `user_disabled` tinyint(1) NOT NULL DEFAULT '0',
  `card_cnt` int(10) NOT NULL DEFAULT '0',
  `xml` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gx_user_cards`
--

CREATE TABLE `gx_user_cards` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `family_num` varchar(255) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `gx_user_cards_tmp`
--

CREATE TABLE `gx_user_cards_tmp` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `card_num` varchar(255) NOT NULL,
  `family_num` varchar(255) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gx_users`
--
ALTER TABLE `gx_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `gx_users_tmp`
--
ALTER TABLE `gx_users_tmp`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `gx_user_cards`
--
ALTER TABLE `gx_user_cards`
  ADD KEY `uid_card` (`user_id`,`card_num`) USING BTREE,
  ADD KEY `uid_family` (`user_id`,`family_num`) USING BTREE;

--
-- Indexes for table `gx_user_cards_tmp`
--
ALTER TABLE `gx_user_cards_tmp`
  ADD KEY `uid_card` (`user_id`,`card_num`) USING BTREE,
  ADD KEY `uid_family` (`user_id`,`family_num`) USING BTREE;


--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);


--
-- Table structure for table `miscs`
--

CREATE TABLE `miscs` (
  `mkey` varchar(255) NOT NULL,
  `mvalue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `miscs`
--
ALTER TABLE `miscs`
  ADD PRIMARY KEY (`mkey`);


--
-- Table structure for table `action_logs`
--

CREATE TABLE `action_logs` (
  `log_id` varchar(100) NOT NULL,
  `log_user_name` varchar(255) NOT NULL,
  `log_action` varchar(150) NOT NULL,
  `log_target` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `log_data` longtext NOT NULL,
  `log_data_old` longtext NOT NULL,
  `log_ip` varchar(100) NOT NULL,
  `log_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_created_by` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_logs`
--
ALTER TABLE `action_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `idx_action_target` (`log_action`,`log_target`) USING BTREE,
  ADD KEY `idx_created_by` (`log_created_by`) USING BTREE,
  ADD KEY `log_created` (`log_created`);
