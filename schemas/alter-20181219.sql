-- alter groups
ALTER TABLE `groups`
	ADD `modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `created_time`,
	ADD `modified_by` INT(11) NOT NULL AFTER `modified`;

ALTER TABLE `groups` CHANGE `created_time` `created_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';

